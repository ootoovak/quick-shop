source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.5'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.0.1'
# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'
# Use Puma as the app server
gem 'puma', '~> 4.1'
# Use SCSS for stylesheets
gem 'sass-rails', '>= 6'
# Transpile app-like JavaScript. Read more: https://github.com/rails/webpacker
gem 'webpacker', '~> 4.0'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.7'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Active Storage variant
# gem 'image_processing', '~> 1.2'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.2', require: false

# An authentication library for signups and logins
gem 'devise'

# An authorisation library
gem 'pundit'

group :development, :test do
  # Use Pry as the REPL for debugging
  gem 'pry-byebug'
  # Use Pry as the REPL for Rails console
  gem 'pry-rails'
  # Test suite and runner
  gem 'rspec-rails', '~> 4.0.0.beta3' # NOTE: Using beta version for Rails 6 compatibility, update ASAP.
  # To generate factories for test data rather than fixtures
  gem 'factory_bot_rails'
  # Linter for Ruby and Rails code
  gem 'rubocop-rails'
  # Linter for RSpec code
  gem 'rubocop-rspec'
  # Static analysis for security vulnerabilities
  gem 'brakeman'
  # Audits the gems in this file for security vulnerabilities
  gem 'bundler-audit'
end

group :test do
  # Helps RSpec system and feature tests interact with web pages
  gem 'capybara'
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
